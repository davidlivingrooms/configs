export BASH_SILENCE_DEPRECATION_WARNING=1

function gpa() {
  git add .
  git commit -a -m "$1"
  git push
}
