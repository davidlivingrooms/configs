" Enable syntax highlighting
syntax on

" Change tabs to 2 spaces
set expandtab 
set tabstop=2
set shiftwidth=2

" Automatically indent when starting new lines in code blocks
set autoindent

" Add line numbers
set number

" shows column, & line number in bottom right 
set ruler

:imap kl <Esc>
noremap ; l
noremap l k
noremap k j
noremap j h
nnoremap ' ;

nnoremap <SPACE> <Nop>
let mapleader = " "

" Copy to clipboard
vnoremap <leader>y "+Y

nnoremap <leader>k <C-d>zz
nnoremap <leader>l <C-u>zz

nnoremap <s-k> <Nop>
vnoremap <s-k> :m '>+1<CR>gv=gv
vnoremap <s-l> :m '<-2<CR>gv=gv


" Color scheme I found that works best with PowerShell
" colorscheme shine

" helpful if using 'set ruler' and 'colorscheme shine', makes lineNumbers grey
" Same example from http://vim.wikia.com/wiki/Display_line_numbers
highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE

" Disable bell sounds 
set noerrorbells visualbell t_vb=
set number relativenumber
