#!/bin/bash

set -e

FILES=("https://gitlab.com/davidlivingrooms/configs/-/raw/master/.ideavimrc" "https://gitlab.com/davidlivingrooms/configs/-/raw/master/.vimrc" "https://gitlab.com/davidlivingrooms/configs/-/raw/master/.bash_profile" "https://gitlab.com/davidlivingrooms/configs/-/raw/master/.zshrc")


for i in ${!FILES[@]}; do
  FILE_NAME="${FILES[$i]#*master\/}"
  echo "Creating ~/$FILE_NAME"
  curl -s ${FILES[$i]} --output ~/$FILE_NAME
done

