# configs
This repo holds configurations for common tools I use.

### Install

```sh
/bin/bash -c "$(curl -fsSL https://gitlab.com/davidlivingrooms/configs/-/raw/master/install.sh)"
```
